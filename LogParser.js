
module.exports.interpretCoordinatorLogAsEventsAndEmit = function(string, reportAs, socket) {
  var match = enlistmentRegex.exec(string);
  var emission = undefined;

  if (match !== null) {
    emission = {type:"enlistment", text:"Enlistment: "+match[2]+" as "+ match[4] ,
      transport:match[1], ip:match[2], port:match[3], serverid:match[4]};
  }

  match = replicationGroupRegex.exec(string);
  if (match !== null) {
    emission = {type:"replicationGroup"}
    emission.text = "ReplicationGroup: " + match[1] +" is now in replication group " + match[2];
    emission.serverid = match[1];
    emission.replicationGroup = match[2];
  }

  match = replicationGroupDisbandedRegex.exec(string);
  if (match !== null) {
    emission = {type:"replicationGroupDisbanded", serverid:match[1], replicationGroup:match[2],
                text:"ReplicationGroupDisbanded: "+match[1]+" has been removed from "+match[2]}
  }

  match = createTableRegex.exec(string);
  if (match != null) {
    emission = {type:"tableCreation", tableName:match[1], tableId:match[2],
                text: "TableCreation: \""+match[1]+"\" mapped to id " + match[2]}
  }

  match = asssignTableRegex.exec(string);
  if (match !== null) {
    emission = {type:"tableAssignment", tableId:match[1], begHash:match[2], endHash:match[3], serverid:match[4],
                text:"\tTableAssignment: table " + match[1] + " assigned to " + match[4]};
  }

  match = requestingSplitTablet.exec(string);
  if (match !== null) {
    emission = {type:"requestingSplitTablet", time:match[1], serverid:match[2], tableid:match[3], hashSplit:match[4],
                text:"Requesting split table " +match[3]+ " on master " + match[2] + " at " + match[4]}
  }

  match = splitTablet.exec(string);
  if (match !== null) {
    emission = {type:"splitTablet", time:match[1], tableid:match[2], hashSplit:match[3],
                text:"Table "+ match[2]+ " split at "+ match[3]}
  }

  match = dropTableRegex.exec(string);
  if (match !== null) {
    emission = {type:"dropTable", time:match[1], tableId:match[3], begHash:match[4], endHash:match[5], serverid:match[2],
                text:"\tdropTable: table " + match[2] + " with ranges " + match[4] + "-" + match[5] + " dropped from " + match[2]};
  }


  match = hintServerCrashedRegex.exec(string);
  if (match !== null) {
    emission = {type:"hintServerCrashed", serverid:match[1], transport:match[2], ip:match[3], port:match[4],
                text:"\tHint server "+ match[1] + "("+match[3]+") crashed"}
  }

  match = serverCrashedRegex.exec(string);
  if (match !== null) {
    emission = {type:"serverCrashed", serverid:match[1],
                text:"ServerCrashed at " + match[1]}
  }

  match = startingRecoveryRegex.exec(string);
  if (match !== null) {
    emission = {type:"startingRecovery", serverid:match[1],
                text:"StartingRecovery of " + match[1]};
  }

  match = startingRecoveryForCrashedServerRegex.exec(string);
  if (match !== null) {
    emission = {type:"startingRecoveryForCrashedServer", recoveryid:match[1], serverid:match[2],
                text:"Starting Recovery " + match[1] + " for crashed server " + match[2]};
  }

  match = recoveryPartitionsRegex.exec(string);
  if (match !== null) {
    tablets = string.match(/tablet\s+.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+/g);

    output = [];
    for (i = 0; i < tablets.length; i++) {
      var lines = tablets[i].split(/[\r\n]+/);
      var jsonTablet = {};
      for (j=1; j<9; j++) {
        matches = lines[j].match(/  ([^ ]+): ([^ ]+)/);
        jsonTablet[matches[1]] = matches[2];
      }

      // serverid transform
      var serverid_raw = jsonTablet["server_id"];
      if (serverid_raw > (Math.pow(2, 32)-1)) {
        var indexNum = serverid_raw&0xFFFFFFFF;
        // We can't just right shift cuz Js has funky 64-bit behavior.
        var generationNum = (serverid_raw-indexNum)/Math.pow(2,32);
        jsonTablet["server_id"] = "" + indexNum + "." + generationNum;
      } else {
        jsonTablet["server_id"] = "" + (serverid_raw&0xFFFFFFFF) +".0";
      }

      output.push(jsonTablet);
    }

    emission = {type:"recoveryPartitions", tablets:output,
                text:"Partition Scheme for Recovery Sent"};
  }

  match = startingRecoveryMasterRegex.exec(string);
  if (match !== null) {
    emission = {type:"startingRecoveryMaster", recoveryid:match[1], serverid:match[2], partition:match[3],
                text:"\tStartingRecoveryMaster " + match[2]};
  }

  match = recoveryMasterFinishedRegex.exec(string)
  if (match !== null) {
    emission = {type:"recoveryMasterFinished", masterId:match[1], numTablets:match[2], numIndexlets:match[3],
                text:"RecoveryMasterFinished " + match[1] + "(" + match[2]+":"+match[3]+")"};
  }

  match = recoveryCompleteRegex.exec(string);
  if (match !== null) {
    emission = {type:"recoveryComplete", recoveryid:match[1], serverid:match[2],
                text:"RecoveryComplete for "+match[2]}
  }

  match = recoveryNotNecessaryRegex.exec(string);
  if (match !== null) {
    emission = {type:"recoveryNotNecessary", serverid:match[1],
              text:"RecoveryNotNecessary for " + match[1]};
  }

  match = serverRemovedRegex.exec(string);
  match = (match === null) ? serverRemovedNoTabletsRegex.exec(string) : match;
  if (match !== null) {
    emission = {type:"serverRemoved", serverid:match[1],
                text:"ServerRemoved: serverid "+match[1]}
  }

  match = serverCrashedSkippedRegex.exec(string);
  if (match !== null) {
    emission = {type:"serverCrashedSkipped", serverid:match[1],
                text:"ServerCrashedSkipped: removing "+match[1]}
  }

  match = coordinatorUpRegex.exec(string);
  if (match !== null) {
    emission = {type:"coordinatorUp",
                text:"CoordinatorUp"};
  }

  if (emission !== undefined) {
    socket.emit(reportAs, emission);
  }
}



// [matching]
var coordinatorUpRegex = new RegExp('CoordinatorService::init.*starting service');

// Here you get an array of [matching, transport, ip, port, serverid]
var enlistmentRegex = new RegExp('Enlisting server at (infrc|fast+udp):host=([^,]*),port=([^ ]*) \\(server id ([^\\)]*)\\)');

// [matching, serverId, replicationGroup]
var replicationGroupRegex = new RegExp("Server ([^ ]*) is now in replication group (.*)");

// [matching, serverId, replicationGroup]
var replicationGroupDisbandedRegex = new RegExp("Removed server ([^ ]*) from replication group (.*)");

// [matching, tableName, (int)tableId]
var createTableRegex = new RegExp("Creating table '([^']*)' with id (.*)");

// [matching, (int)tableId, (hex)begHash, (hex)endHash, serverID]
var asssignTableRegex = new RegExp("Assigning table id ([^,]*), key hashes ([^-]*)-([^,]*), to master (.*)")

// [matching, time, serverid, tableid, hashSplit]
var requestingSplitTablet = new RegExp("([^ ]*) .*Requesting master ([^ ]+) to split table id ([^ ]+) at key hash (.*)");

// [matching, time, tableName, keyHashInt]
var splitTablet = new RegExp("([^ ]*) .*In table ([^ ]+) I split the tablet at key (.*)");

// [matching, time, serverid, tableid, (hex)beghash, (hex)endhash]
var dropTableRegex = new RegExp("([^ ]*) .*Requesting master ([^ ]*) to drop table id ([^,]+), key hashes ([^-]*)-(.*)");

// [matching], severID, transport, ip, port
var hintServerCrashedRegex = new RegExp("hintServerCrashed .* Checking server id ([^ ]*) \\((infrc|fast+udp):host=([^,]*),port=([^)]*)\\)");

// [matching, serverID]
var serverCrashedRegex = new RegExp("Server id ([^ ]*) has crashed, notifying");

// [matching, serverid]
var startingRecoveryRegex = new RegExp("Starting recovery of server ([^ ]*)")

// [matching, recoveryid, serverid(of crashed maser)]
var startingRecoveryForCrashedServerRegex = new RegExp("Recovery::performTask .* Starting recovery ([^ ]+) for crashed server (.*)");

// [matching, recoveryid, serverid(recovering to), partition]
var startingRecoveryMasterRegex = new RegExp("Starting recovery ([^ ]+) on recovery master ([^,]+), partition (.+)");

// [tableid, semi-json thing]
var recoveryPartitionsRegex = new RegExp("Partition Scheme for Recovery:");

// [matching, masterid, numTablets, numIndexlets]
var recoveryMasterFinishedRegex = new RegExp("recoveryMasterFinished .*: Called by masterId (\d+\.\d+) with (\d+) tablets and (\d+) indexlets");

// [matching, recoveryid, serverid]
var recoveryCompleteRegex = new RegExp("Recovery ([^ ]*) completed for master (.*)");

// [matching, serverid]
var recoveryNotNecessaryRegex = new RegExp("No tablets in crashed server ([^,]*), removing it from coordinator server list")

// [matching, serverid]
var serverRemovedRegex = new RegExp("Removing server ([^ ]*) from cluster/coordinator server list");

// [matching, serverid]
var serverRemovedNoTabletsRegex = new RegExp("No tablets in crashed server ([^,]*), removing it from coordinator server list");

// [matching, serverid]
var serverCrashedSkippedRegex = new RegExp("Skipping serverCrashed for server ([^:]*)");

