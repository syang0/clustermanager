var socket = io.connect('http://localhost');

// {origin, logData}
var serverLogs = [];

var escapePeriods = function (string) {
  return string.split(".").join("p");
}

socket.on('reconnecting', function(number) {
  $("#disconnectEvent").modal({backdrop: 'static', keyboard: false});
  $("$reconnectAtempt").html("Attempting Reconnect... ("+number+")")
  $("reconnectOkay").button('loading');
});

socket.on('reconnect', function() {
  $("$reconnectAtempt").html("Reconnected!")
  $("reconnectOkay").button('reset');
});


socket.on("serverLogData", function(data) {
  var originEscaped = escapePeriods(data.origin);
  var prev = serverLogs.find(function(ele, i, arr) {
    return ele.origin === data.origin;
  });

  if (prev !== undefined) {
    prev.logData = prev.logData + data.text;
    var pre = $("#collapse"+originEscaped+" .accordion-inner pre");
    pre.append(data.text);
    pre.scrollTop(pre.prop("scrollHeight"));
    return;
  }


  serverLogs.push({origin:data.origin, logData:data.text});
  serverLogs.sort(function(a, b) { return a.origin.localeCompare(b.origin)});
  $("#serverLogArea").html("");

  for (i in serverLogs) {
    var originEscaped = escapePeriods(serverLogs[i].origin);
    var log = serverLogs[i].logData;

    $("#serverLogArea").append(
      "<div class='accordion-group'>"+
        "<div class='accordion-heading'>"+
          "<a class='accordion-toggle' data-toggle='collapse' data-parent='#serverLogArea' href='#collapse"+originEscaped+"'>"+
            serverLogs[i].origin +
          "</a>" +
        "</div>" +
        "<div id='collapse"+originEscaped+"' class='accordion-body collapse in'>"+
          "<div class='accordion-inner'>" +
            "<pre class='pre-scrollable'>"+log+"</pre>"+
          "</div>"+
        "</div>" +
      "</div>"
    );

    $(".collapse").collapse('hide');
  }

});

socket.on('logData', function(data) {
  var log = $("#log");
  log.append("\n"+data.text);
  log.scrollTop(log.prop("scrollHeight"));
});

socket.on('eventsLog', function (data) {
  translateEventLogToAnimations(data);
  var log = $("#eventsLog");
  log.append(data.text+"\n");;
  log.scrollTop(log.prop("scrollHeight"));
});

var checkForConfigs = function () {
  // Call back for when the server acks your configurations.
  var configsSetCallback = function(data) {
    if (data.success) {
          $('#coordinatorConfigurations').modal('hide');
          var username = $('#inputUsername').val();
          var password = $('#inputPassword').val();
          var ramcloudDir = $('#inputRAMCloudBinDir').val();
          localStorage.setItem('inputUsername', username);
          localStorage.setItem('inputPassword', password);
          localStorage.setItem('inputRAMCloudBinDir', ramcloudDir);
        } else {
          $('#coordinatorConfigurations').modal({backdrop: 'static', keyboard: false});
          $("#configErrors").html("Verfication Failed: " + data.reason);
          $("#configErrors").show();
        }
        $('#saveInitialConfigs').button('reset');
  }

  // Set what happens when you click 'verify' on the form
  $('#saveInitialConfigs').click( function() {
    $('#saveInitialConfigs').button('loading');
    $("#configErrors").hide();
    var username = $('#inputUsername').val();
    var password = $('#inputPassword').val();
    var ramcloudDir = $('#inputRAMCloudBinDir').val();

    socket.emit('setConfigs', {username:username, password:password, ramcloudDir:ramcloudDir});
    socket.on('configsSet', configsSetCallback);
  });

  // Prepopulate form
  var username = localStorage.getItem('inputUsername');
  var password = localStorage.getItem('inputPassword');
  var ramcloudDir = localStorage.getItem('inputRAMCloudBinDir');
  $('#inputUsername').val(username);
  $('#inputPassword').val(password);
  $('#inputRAMCloudBinDir').val(ramcloudDir);

  // If information is missing, show prompt, else just submit in the background and if fails, then prompt.
  if (username === null || password === null || ramcloudDir === null) {
    $("#configErrors").hide();
    $('#coordinatorConfigurations').modal({backdrop: 'static', keyboard: false});
  } else {
     socket.emit('setConfigs', {username:username, password:password, ramcloudDir:ramcloudDir});
     socket.on('configsSet', configsSetCallback);
  }
};

var filloutServerIdDropdowns = function(beg, end) {
  $(".inputServerId").html("");
   // Give server id options
  for (i=beg; i<=end; i++) {
    $(".inputServerId").append("<option value=\""+i+"\">"+i+"</option>");
  }

  $(".inputServerIdFull").html("");
  for (i=1; i<=80; i++) {
    $(".inputServerIdFull").append("<option value=\""+i+"\">"+i+"</option>");
  }
}

var onCoordinatorServerIdSelected = function() {
  $("#coordinatorStatus").html("Checking...");
  pollCoordinatorStatus();
}

var pollCoordinatorStatus = function() {
  var serverNum = $("#cooordinatorServerId").val();
  socket.emit('checkServerStatus', {beg:serverNum, end:serverNum, emitAs:"coordinatorSelectedStatus"});

  socket.on('coordinatorSelectedStatus', function(data) {
    if (data[0].status.match("Connection timed out")) {
      var result = '<font color="purple">Unreachable</font>';
    } else {
      var result = (data[0].services.match("Coordinator")) ? '<font color="red">Already Running</font>' : '<font color="green">Okay to use<font>' ;
    }
    $("#coordinatorStatus").html(result);
  });
}

$(document).ready(function() {
  filloutServerIdDropdowns(1, 80);

  $("#startCoordinator").click(startCoordinatorClicked);
  $("#killCoordinator").click(killCoordinatorClicked);

  $("#startMasters").click(startMastersClicked);
  $("#killMasters").click(killMastersClicked);

  // Random buttons
  $("#start").click(startClicked);
  $('#checkStatus').click(function() {socket.emit('checkStatus')});
  $('#clearLog').click(function() {$("#log").html("Log Area")});
  $('#clearEventsLog').click(function() {$("#eventsLog").html("")});

  setInterval(pollCoordinatorStatus, 1000);


  // Initial Configs
  checkForConfigs();
  $("#logout").click(function() {
    localStorage.removeItem('inputPassword');
    location.reload();
  });

  //Tablet Buttons
  $('#buttonCreateTable').click(function() {
    var name = $("#inputTableName").val();
    var serverSpan = parseInt($("#inputServerSpan").val());
    if (serverSpan.length === 0) {
      socket.emit('clientCall', [{opcode:"createTable", table:name}]);
    } else {
      socket.emit('clientCall', [{opcode:"createTable", table:name, serverSpan:serverSpan}]);
    }
  });

  $('#buttonDropTable').click(function() {
    var name = $("#inputTableName").val()
    socket.emit('clientCall', [{opcode:"dropTable", table:name}]);
  });

  $('#buttonSplitHash').click(function() {
    var name = $("#inputTableName").val();
    var splitHash = $("#inputSplitHash").val();

    socket.emit('clientCall', [{opcode:"splitTable", table:name, splitHash:splitHash}]);
  });

  $('#verifyRAMCloudBin').on('click', function () {
    var $btn = $(this).button('loading')

  });


  $('#pollCluster').click(function() {
    socket.emit('checkServerStatus', {beg:$("#masterPollIdBeg").val(),end:$("#masterPollIdEnd").val()})
    $("#pollCluster").button('loading');
  });


  socket.on('checkServerStatusReturn', function(list) {
    $("#pollCluster").button('reset');
    var statusList = $("#statusList")
    statusList.html("");

    list.forEach(function(entry) {
    var output = "<tr>"
        + "<td>"+entry.hostname+"</td>"
        + "<td>"+entry.status+"</td>"
        + "<td>"+entry.services+"</td>"
        + "<td>"+entry.user+"</td>"
        + "<td>"+entry.rcresInfo+"</td>"
        + "<td></td>"
        +"</tr>";
      statusList.append(output);
    });
  });
});

var startClicked = function() {
  console.log("emitted");
  socket.emit('startCluster');
};


var startMastersClicked = function() {
  var coordinator = $("#cooordinatorServerId").val();
  var transport = $("#inputTransport").val();
  var clusterName = $("#inputClusterName").val();
  var coordinatorPort = $("#inputCoordinatorPort").val();
  var clientPort = $("#inputClientPort").val();
  var zooKeeper = $("#inputZk").val();
  var additionalArgs = $("#inputAdditionalCoordinatorArgs").val();

  var serverRangeBeg = $("#masterServerIdBeg").val();
  var serverRangeEnd = $("#masterServerIdEnd").val();
  var replicas = $("#inputReplicas").val();

  var services = {masterService:$("#masterServiceChecked").is(':checked'),
                  backupService:$("#backupServiceChecked").is(':checked')};

  if (replicas < 0 || serverRangeBeg > serverRangeEnd) {
    alert("There is an error in the master configurations. Check range or replicas.");
  } else {
    var options = {coordinator:coordinator, coordinatorPort:coordinatorPort, clusterName:clusterName,
                   transport:transport, serverStart:serverRangeBeg, serverEnd:serverRangeEnd,
                   replicas:replicas, services:services, clientPort:clientPort,
                   zooKeeper:zooKeeper, additionalArgs:additionalArgs};

    socket.emit("startServer", options);
  }
}

var killMastersClicked = function() {
  var serverRangeBeg = $("#masterServerIdBeg").val();
  var serverRangeEnd = $("#masterServerIdEnd").val();

  socket.emit('killServers', {serverStart:serverRangeBeg, serverEnd:serverRangeEnd});
}

var killCoordinatorClicked = function() {
  socket.emit("killCoordinator", {server:$("#cooordinatorServerId").val()});
}

var startCoordinatorClicked = function() {
  clusterVisualizer.reset();
  var coordinator = $("#cooordinatorServerId").val();
  var transport = $("#inputTransport").val();
  var clusterName = $("#inputClusterName").val();
  var coordinatorPort = $("#inputCoordinatorPort").val();
  var zooKeeper = $("#inputZk").val();
  var additionalArgs = $("#inputAdditionalCoordinatorArgs").val();

  var options = {server:coordinator, transport:transport, clusterName:clusterName, port:coordinatorPort, zooKeeper:zooKeeper, additionalArgs:additionalArgs};
  // Start Coordinator
  socket.emit("startCoordinator", options);

  // Close Modal
  //$("#coordinatorConfigurations").modal('hide');
}

// objects should be in {ip,serverid,status,replicationGroup}
var clusterVisualizer = (function() {
  var selector = "#serverList";

  // {ip:ip, serverid:serverid, status:status, replicationGroup:replicationGroup, tablets:[{name:name, id:tabletId, begHash:begHash, endHash:endHash, state:(active,reovering)}]}
  var clusterState = [];

  // {recoveryid:{serverid,partitions(raw),recoveryMasters:[partitionids]}}
  var recoveries = {};
  var serverid2Recoveryid ={}

  var findServerByid = function(serverid) {
    for (i in clusterState) {
      if (clusterState[i].serverid == serverid) {
        return i;
      }
    }
    return -1;
  }

  var findServerIndexByIp = function(serverip) {
    for (i in clusterState) {
      if (clusterState[i].ip == serverip) {
        return i;
      }
    }

    return -1;
  }

  return {
    reset:function() {
      serverid2Recoveryid = {};
      clusterState = [];
      recoveries = {};
      this.redraw();
    },

    startingRecoveryForCrashedServer:function(recoveryid, serverid) {
      serverid2Recoveryid[serverid] = recoveryid;
      this.updateStatus(severid, "RECOVERING");
    },

    setRecoveryPartitions:function(partitions) {
      var serveridToRecover = partitions[0].server_id;
      var recoveryid =  serverid2Recoveryid[serveridToRecover];
      recoveries[recoveryid] = {partitions:partitions,
                                 recoveryMasters:{},
                                 serverid:serveridToRecover
                               };
    },

    startingRecoveryMaster:function(recoveryid, recoveringToId, partitionid) {
      var recovery = recoveries[recoveryid];
      if (recovery === undefined) {
        console.log("Bug in startingRecoveryMaster.... Should have had master added from setRecoveryPartitions");
        return;
      }

      var prevMasterRecord = recovery.recoveryMasters[recoveringToId];
      if (prevMasterRecord === undefined) {
        recovery.recoveryMasters[recoveringToId] = [partitionid];
      } else {
        prevMasterRecord.push(partitionid)
      }
    },

    recoveryMasterFinished:function(recoveryMasterId) {
      //Do onthing for now
    },

    recoveryComplete:function(recoveryid, serverid) {
      var recovery = recoveries[recoveryid];
      if (recovery === undefined) {
        console.log("competeRecovery: Nothing to complete");
        return;
      }

      // For every master
      for (master in recovery.recoveryMasters) {
        // For every partition owned by master
        for (i = 0; i < recovery.recoveryMasters[master].length; i++) {
          var partitionid = recovery.recoveryMasters[master][i];
          // for every instance of the partition in the recovery partitions map...
          for (j=0; j < recovery.partitions.length; j++) {
            var partition = recovery.partitions[j];
            if (partition["user_data"] == partitionid) {
              // Tansform to hex before storing
              var begHash = UnsignedLong(partition.start_key_hash, 10).stringify();
              var endHash = UnsignedLong(partition.end_key_hash, 10).stringify();
              this.addTablet(master, partition.table_id, begHash, endHash);
            }
          }

        }
      }

      delete serverid2Recoveryid[serverid];
      delete recoveries[recoveryid];
    },

    setServer:function(ip, serverid, status, replicationGroup) {
      var prevIndex = findServerIndexByIp(ip);
      var newObj = {ip:ip, serverid:serverid, status:status, replicationGroup:replicationGroup, tablets:[]}
      if (prevIndex === -1) {
        clusterState.push(newObj)
      } else {
        clusterState[i] = newObj;
      }

      this.redraw();
    },

    updateStatus:function(serverid, status) {
      var prevIndex = findServerByid(serverid);
      if (prevIndex >= 0) {
        clusterState[prevIndex].status = status;
      }

      this.redraw();
    },

    addTablet:function(serverid, tabletId, begHash, endHash) {
      var prevIndex = findServerByid(serverid);
      if (prevIndex >= 0) {
        var tablet = {name:name, id:tabletId, begHash:begHash, endHash:endHash, state:"active"}
        clusterState[prevIndex].tablets.push(tablet);
      }

      this.redraw();
    },

    splitTablet:function(serverid, tableid, hashSplit) {
      var split = UnsignedLong(hashSplit, 16);
      var prevIndex = findServerByid(serverid);
      if (prevIndex >= 0) {
        var tablets = clusterState[prevIndex].tablets;
        for (var i in tablets) {
          var tablet = tablets[i];
          if (tablet.id == tableid) {
            // TODO(syang0) store the beg and end hash as such so I don't need to reparse every time a split happens.
            var begHash = UnsignedLong(tablet.begHash, 16)
            var endHash = UnsignedLong(tablet.endHash, 16)

            if (split.compare(begHash) >= 0 && split.compare(endHash) <= 0) {
              var newTablet1 = {name:tablet.name, id:tableid, begHash:tablet.begHash, endHash:hashSplit};
              var newTablet2 = {name:tablet.name, id:tableid, begHash:hashSplit, endHash:tablet.endHash};

              tablets.splice(i, 1);
              tablets.push(newTablet1);
              tablets.push(newTablet2);
              this.redraw();
              return;
            }
          }
        }
    }

      this.redraw();
    },

    dropTablet:function(serverid, tabletId, begHash, endHash) {
      var prevIndex = findServerByid(serverid);
      if (prevIndex >= 0) {
        var server = clusterState[prevIndex];
        for (i in server.tablets) {
          var tablet = server.tablets[i];
          if (tablet.id == tabletId)
            server.tablets.splice(i, 1);
        }
      }

      this.redraw();
    },

    updateReplicationGroup:function(serverid, replicationGroup) {
      var prevIndex = findServerByid(serverid);
      if (prevIndex >= 0) {
        clusterState[prevIndex].replicationGroup = replicationGroup;
      }

      this.redraw();
    },

    redraw:function() {
      clusterState.sort(function(a, b) {
        if (a.ip < b.ip)
          return -1;
        if (a.ip > b.ip)
          return 1;
        return 0;
      });
      var viz = $(selector).html("");
      for (i in clusterState) {
        optBeg = ""; optEnd = "";
        var server = clusterState[i];
        var serverIdEscaped = server.serverid.replace(".", "point");
        // var output = "<p id=\""+serverIdEscaped+"\">["+server.ip+"][Serverid:"+server.serverid+"][Status:"+server.status+"][ReplicationGroup:"+server.replicationGroup+"]</p>";

        var tablet_output = "";
        for (i in server.tablets) {
          var tablet = server.tablets[i];
          tablet_output += "<p>"+tablet.id + ": <small>" + tablet.begHash + "-" + tablet.endHash +"</small></p>";
        }

        // cross things off if server is not up
        if (server.status != "UP") {
          optBeg += "<font color=\"#B8B8B8\">"
          optEnd += "</font>"
        }
        var output = "<tr id=\""+serverIdEscaped+"\">"
          + "<td>"+server.ip+"</td>"
          + "<td>"+server.serverid+"</td>"
          + "<td>"+server.status+"</td>"
          + "<td>"+optBeg+server.replicationGroup+optEnd+"</td>"
          + "<td>"+optBeg+tablet_output+optEnd+"</td>"
          + "<td>"+optBeg+optEnd+"</td>"
          + "<td></td>"
          +"</tr>"
        viz.append(output);
      }
    }
  };
})();


var translateEventLogToAnimations = function(data) {
  switch (data.type) {
    case "enlistment":
      clusterVisualizer.setServer(data.ip, data.serverid, "UP", "?");
      break;
    case "replicationGroup":
      clusterVisualizer.updateReplicationGroup(data.serverid, data.replicationGroup);
      break;

    case "replicationGroupDisbanded":
      clusterVisualizer.updateReplicationGroup(data.serverid, "?");
      break;

    case "serverCrashed":
      clusterVisualizer.updateStatus(data.serverid, "CRASHED");
      break;

    case "serverRemoved":
    case "serverCrashedSkipped":
      clusterVisualizer.updateStatus(data.serverid, "REMOVED");
      break;

    case "tableAssignment":
      clusterVisualizer.addTablet(data.serverid, data.tableId, data.begHash, data.endHash)
      break;
    case "dropTable":
      clusterVisualizer.dropTablet(data.serverid, data.tableId);
      break;


      // Recovery Related
    case "startingRecoveryForCrashedServer":
      clusterVisualizer.startingRecoveryForCrashedServer(data.recoveryid, data.serverid)
      break;

    case "recoveryPartitions":
      clusterVisualizer.setRecoveryPartitions(data.tablets);

      break;

    case "startingRecoveryMaster":
      clusterVisualizer.startingRecoveryMaster(data.recoveryid, data.serverid, data.partition)
      break;

    case "recoveryMasterFinished":
      // do nothing for now
      break;

    case "requestingSplitTablet":
      // for now
      clusterVisualizer.splitTablet(data.serverid, data.tableid, data.hashSplit);
      break;

    case "splitTablet":
      break;

    case "recoveryComplete":
      clusterVisualizer.recoveryComplete(data.recoveryid, data.serverid);
      break;
  }
}













