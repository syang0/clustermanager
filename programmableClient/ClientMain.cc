/* Copyright (c) 2009-2014 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <iostream>
#include <fstream>
#include <string>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
using namespace rapidjson;


#include "RamCloud.h"
#include "TableEnumerator.h"


using namespace RAMCloud;

#define GET_64(name, doc) doc->FindMember(name)->value.GetInt64()
#define GET_32(name, doc) doc->FindMember(name)->value.GetInt()
#define GET_STR(name, doc) doc->FindMember(name)->value.GetString()

// Options are
// {"opcode":"createTable", (table:"tableName" | tableId:"1"), serverSpan:"optionalInt"}
// {"opcode":"droptable", (table:"tableName" | tableId:"1")}
// {"opcode":"migrateTablet", (table:"tableName" | tableId:"1"), firstKeyHash, lastKeyhash, serverId:"ToServerid"}
// {"opcode":"splitTable", table:"tablename", splitHash:uint64_t}


// {"opcode":"read", table:"tableName", key:"key", value:"value"}
// {"opcode":"write", table:"tableName", key:"key" }
// {"opcode":"multiWrite", "table":"tablename", "keyStart":uint32_t, "numObjects":uint32_t, "keyLength":uint16_t, "valueLength":uint32_t}
// {"opcode":"multiRead", "table":"tablename", "keyStart":uint32_t, "numObjects":uint32_t, "keyLength":uint16_t, "valueLength":uint32_t, "verify":"true"|"false"}

static void printHelp() {
  printf("Usage:  \r\n");
  printf("./Client <RamCloud Coordinator Locator> <JSON file to execute>\r\n");
  printf("Example: ./Client infrc:host=192.168.0.175,port=12246 options.json\r\n");
}

int main(int argc, char *argv[])
    try
{
  if (argc < 3) {
    printHelp();
    exit(-1);
  }

  // Set line buffering for stdout so that printf's and log messages
  // interleave properly.
  setvbuf(stdout, NULL, _IOLBF, 1024);

  //TODO(syang0): parameterize this shit.
  Context context(false);
  context.transportManager->setSessionTimeout(500);

  string locator = argv[1];
  RamCloud ramcloud(&context, locator.c_str(), "__unamed__");


  std::ifstream inputFile(argv[2]);
  string json;
  while (!inputFile.eof()) {
    json.push_back(inputFile.get());
  }
  json = json.substr(0, json.length()-1); // gets rid of EOF character
  inputFile.close();

  Document operations;
  operations.Parse(json.c_str());

  int currOp = 0;
  assert(operations.IsArray());
  int numOps = operations.Capacity();
  printf("Found %d operations\n", numOps);
  for (Document::ValueIterator op = operations.Begin();
          op != operations.End(); ++op) {
    assert(op->IsObject());
    ++currOp;

    const char *opcode = GET_STR("opcode", op);
    printf("Running opcode %s (%d of %d)\r\n\t",  opcode, currOp, numOps);

    if (0 == strcmp(opcode, "createTable")) {
      const char *name = GET_STR("table", op);
      if (op->HasMember("serverSpan")) {
          uint32_t serverSpan = GET_32("serverSpan", op);
          printf("Creating Table createTable(%s, %u)\r\n", name, serverSpan);
          ramcloud.createTable(name, serverSpan);
      } else {
          printf("Creating Table - createTable(%s)\r\n", name);
          ramcloud.createTable(name);
      }
    } else if (0 == strcmp(opcode, "dropTable")) {
      const char *name = GET_STR("table", op);
      printf("Dropping Table - dropTable(%s)\r\n", name);
      ramcloud.dropTable(name);
    } else if (0 == strcmp(opcode, "migrateTablet")) {
      uint64_t tableId;
      if(!op->HasMember("tableId")) {
        assert(op->HasMember("table"));
        tableId = ramcloud.getTableId(GET_STR("table", op));
      } else {
        tableId = GET_64("tableId", op);
      }

      uint64_t firstKeyHash = GET_64("firstKeyHash", op);
      uint64_t lastKeyHash = GET_64("lastKeyHash", op);
      Document::ValueIterator serverid = op->FindMember("serverid")->value.Begin();
      uint32_t generationNumber = GET_32("generation", serverid);
      uint32_t indexNumber = GET_32("index", serverid);

      ServerId newOwner(indexNumber, generationNumber);
      printf("migrateTable(%u, %lu, %lu, (%u.%u)\r\n", tableId, firstKeyHash,
              lastKeyHash, indexNumber, generationNumber);
      ramcloud.migrateTablet(tableId, firstKeyHash, lastKeyHash, newOwner);

    } else if (0 == strcmp(opcode, "splitTable")) {
      const char *name = GET_STR("table", op);
      const char *splitHash_in = GET_STR("splitHash", op);
      uint64_t splitHash;
      std::stringstream ss;
      ss << std::hex << splitHash_in;
      ss >> splitHash;

      ramcloud.splitTablet(name, splitHash);
      printf("splitTable(%s,%lu)\n", name, splitHash);
    } else if (0 == strcmp(opcode, "multiRead")) {
      uint64_t tableId;
      if(!op->HasMember("tableId")) {
        assert(op->HasMember("table"));
        tableId= ramcloud.getTableId(GET_STR("table", op));
      } else {
        tableId = GET_64("tableId", op);
      }

      uint32_t keyStart = GET_32("keyStart", op);
      uint32_t numObjects = GET_32("numObjects", op);
      uint32_t valueLength = GET_32("valueLength", op);
      uint16_t keyLength = (uint16_t) GET_32("keyLength", op);


      MultiReadObject** objects = static_cast<MultiReadObject**>(
                                    malloc(sizeof(MultiReadObject*)*numObjects));
      Tub<ObjectBuffer>* values = static_cast<Tub<ObjectBuffer>*>(malloc(sizeof(Tub<ObjectBuffer>)*numObjects));

      char *keys = static_cast<char*>(malloc(numObjects*(keyLength+1)));

      if (objects == NULL || keys == NULL || values == NULL) {
        printf("multiWrite failed - couldn't malloc arrays of MultiWrite objects, keys, and/or values");
      }

      for (uint32_t i = 0; i < numObjects; i++) {
        char *key = keys + i*(keyLength+1);
        snprintf(key, keyLength+1, "%dp%0*d", keyStart+i, keyLength, 0);
        if (i == 0) {
          printf("sampleKey: %s\r\n", key);
        }

        objects[i] = new MultiReadObject(tableId, key, keyLength, &(values[i]));
      }

      printf("multiRead([tableId=%u, key=%u x %u of length %u], numObjects=%u\r\n",
        tableId, keyStart, numObjects, keyLength, numObjects);

      ramcloud.multiRead(objects, numObjects);

    } else if (0 == strcmp(opcode, "multiWrite")) {
      uint64_t tableId;
      if(!op->HasMember("tableId")) {
        assert(op->HasMember("table"));
        tableId= ramcloud.getTableId(GET_STR("table", op));
      } else {
        tableId = GET_64("tableId", op);
      }

      uint32_t keyStart = GET_32("keyStart", op);
      uint32_t numObjects = GET_32("numObjects", op);
      uint32_t valueLength = GET_32("valueLength", op);
      uint16_t keyLength = (uint16_t) GET_32("keyLength", op);

      char *keys = static_cast<char*>(malloc(numObjects*(keyLength+1)));
      char *values = static_cast<char*>(malloc(numObjects*(valueLength+1)));

      MultiWriteObject** objects = static_cast<MultiWriteObject**>(
                                    malloc(sizeof(MultiWriteObject*)*numObjects));

      if (objects == NULL || keys == NULL || values == NULL) {
        printf("multiWrite failed - couldn't malloc arrays of MultiWrite objects, keys, and/or values");
      }

      for (uint32_t i = 0; i < numObjects; i++) {
        char *key = keys + i*(keyLength+1);
        char *value = values + i*(valueLength+1);
        snprintf(key, keyLength+1, "%dp%0*d", keyStart+i, keyLength, 0);
        snprintf(value, valueLength+1, "%dp%0*d", keyStart+i, valueLength, 0);
        if (i == 0) {
          printf("sampleKey: %s\r\n", key);
        }
        objects[i] = new MultiWriteObject(tableId, key, keyLength, value, valueLength);
      }

      printf("mutliWrite([tableId=%u, key=%u x %u of length %u, valueLength=%u], numObjects=%u\r\n",
        tableId, keyStart, numObjects, keyLength, valueLength, numObjects);

      ramcloud.multiWrite(objects, numObjects);

      free(keys);
      free(values);
      for (uint32_t i = 0; i < numObjects; i++)
        delete (objects[i]);
      free(objects);
    } else if (0 == strcmp(opcode, "multiRemove")) {

    } else if (0 == strcmp(opcode, "multiIncrement")) {

    } else if (0 == strcmp(opcode, "read")) {
      uint64_t tableId;
      if(!op->HasMember("tableId")) {
        assert(op->HasMember("table"));
        tableId= ramcloud.getTableId(GET_STR("table", op));
      } else {
        tableId = GET_64("tableId", op);
      }

      const char *key = GET_STR("key", op);
      uint16_t keylen = strlen(key);
      Buffer value;

      printf("read(%lu, %s, %u)\r\n", tableId, key, keylen);
      ramcloud.read(tableId, key, keylen, &value);
      string strVal = string(reinterpret_cast<const char*>(
                        value.getRange(0, value.size())),
                        value.size());
      printf("\t\tResult:%s\r\n", strVal.c_str());

    } else if (0 == strcmp(opcode, "write")) {
      uint64_t tableId;
      if(!op->HasMember("tableId")) {
        assert(op->HasMember("table"));
        tableId= ramcloud.getTableId(GET_STR("table", op));
      } else {
        tableId = GET_64("tableId", op);
      }

      const char *key = GET_STR("key", op);
      uint16_t keylen = strlen(key);
      const char *value = GET_STR("value",op);
      uint64_t vallen = strlen(value);

      printf("write(%lu, %s, %u, %s, %lu)\r\n", tableId, key, keylen, value, vallen);
      ramcloud.write(tableId, key, keylen, value, vallen);
    } else {
      fprintf(stderr, "Nothing matches...");
    }
  }



    return 0;
} catch (RAMCloud::ClientException& e) {
  fprintf(stderr, "RAMCloud exception: %s\n", e.str().c_str());
  return 1;
} catch (RAMCloud::Exception& e) {
  fprintf(stderr, "RAMCloud exception: %s\n", e.str().c_str());
  return 1;
}