
var Connection = require('ssh2')
var express = require('express')
var exphbs  = require('express-handlebars');
var app = express();
var sys = require('sys');
var child = require('child_process');
var fs = require('fs');

var LogParser = require("./LogParser");

var config = require ("./config.json");


app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.render('clusterManager');
});

app.get('/clusterManager', function(req, res) {
  res.render('clusterManager');
});

app.get('/start', function (req, res) {
  console.log("in start");
});

app.get('/end', function (req, res) {
  res.send("hello world!")
});

var port = 3000;
if (config.port) {
  port = config.port;
}

var server = app.listen(port, function() {
  var host = server.address().address
  var port = server.address().port

  console.log('Example app listening at http://%s:%s', host, port);
})

var io = require ('socket.io')(server)
io.on('connection', function (socket) {
  console.log("Connection! "+socket.id);
  var coordinatorLocator = "";

  // var ramcloudBinDir = undefined;
  var username = undefined;
  var password = undefined;
  var ramcloudBinDir = undefined;
  var clientDir = undefined;
  var clientExecutable = undefined;

  socket.on('setConfigs', function(data) {
    username = data.username;
    password = data.password;
    ramcloudBinDir = data.ramcloudDir + "/";

    runAndCallBack("ls " + ramcloudBinDir + "libramcloud.a && ls " + ramcloudBinDir + "coordinator && ls " + ramcloudBinDir + "server ",
                  "rcmaster", username, password,
      function (log) {
        var textLog = ""+log;
        console.log(textLog);

        emission = {success:true, reason:"no failures"}
        if ((/Authentication failure/.exec(textLog)) !== null) {
          emission = {success:false, reason:"Authentication Failure - <strong>Check your username and password.</strong><br>Remember, password is the string in ~/PASSWD.txt on rcmaster (i.e. the text when you run \"cat ~\PASSWD.txt\" on rcmaster"};
          socket.emit("configsSet", emission);
          return;
        }


        if (textLog.match(/No such file or directory/) !== null) {
          emission = {success:false, reason:"RamCloud Bin - Either your Ramcloud bin directory above is incorrect OR you have not complied RAMCloud. <br><br>(Try sshing into rcmaster, cd the ramcloud directory, and make.<br>If that fails, make sure the full path above is accessible by cd-ing it.)."};
          socket.emit("configsSet", emission);
          return;
        }

        if (textLog.match(/Permission denied/) !== null) {
          emission = {success:false, reason:"Permission Deined - Check that the Ramcloud bin directory is correct AND you have permission to read/exectue."}
          socket.emit("configsSet", emission);
          return;
        }

        var clientSrcDir = __dirname + "/programmableClient/"
        clientDir = clientSrcDir + "/clientBuilds/" + username + "/";
        clientExecutable = clientDir+"Client";

        var makeCmd = "g++ --std=c++0x -g -DNDEBUG -L"+ramcloudBinDir+" -lramcloud -I"+ramcloudBinDir+"/../src  -I"+ramcloudBinDir+" -I"+ramcloudBinDir
        + "/../gtest/include  -I"+clientSrcDir+"/rapidjson/include -I. "+clientSrcDir+"/ClientMain.cc -o "+clientExecutable+" -Wl,-rpath="+ramcloudBinDir;

        if (!fs.existsSync(clientDir)) {
           fs.mkdirSync(clientDir);
         }

        runAndCallBackWhenDone(makeCmd, "rcmaster", username, password, function(error, stdout, stderr) {
          if (error !== null || stdout.length !== 0 || stderr.length !== 0) {
            var combinedLog = error + "####" + stdout + "####" + stderr;
            console.log(combinedLog)
            emission.success = false;
            emission.reason= "Client build failed. <br><br>Command used was:<br>" + makeCmd + "<br><br>Error was:<br>" +combinedLog;
          }
          socket.emit("configsSet", emission);
        });
      });
  });

  socket.on('startCoordinator', function(data) {
    var server = "00"+data.server;
    server = "192.168.0.1"+server.substr(server.length-2);

    //TODO(syang0) should do check clustername valididty.

    var additionalArgs = " " + data.additionalArgs;
    if (data.zooKeeper.length > 0) {
      additionalArgs += " -x " + data.zooKeeper;
    }

    coordinatorLocator = data.transport+":host="+server+",port="+data.port;
    var runCmd = ramcloudBinDir+"coordinator"
      + " -C " + coordinatorLocator
      + " --clusterName " + data.clusterName +
      additionalArgs;

    coordinatorLogParserAndReporter(runCmd, server, username, password, "logData", socket);
  });

  socket.on('killCoordinator', function(data) {
    var server = "00"+data.server;
    server = "192.168.0.1"+server.substr(server.length-2);

    var runCmd = "killall -9 coordinator";
    runAndEmit(runCmd, server, username, password, "logData", socket);
  });

  // data should be an array of [{server, services, transport, coordinator, clustername, replicas, port}]
  socket.on('startServer', function(options) {
    console.log("Starting Servers");
    for (i=options.serverStart; i<=options.serverEnd; i++) {
      var server = "00"+i;
      server = "192.168.0.1"+server.substr(server.length-2);

      var coordinator = "00"+options.coordinator;
      coordinator = "192.168.0.1"+coordinator.substr(coordinator.length-2);

      var services;
      if (options.services.masterService && options.services.backupService) {
        services = "";
      } else if (options.services.masterService) {
        services = " --masterOnly "
      } else if (options.services.backupService) {
        services = " --backupOnly "
      }

      var replicas = " -r " + options.replicas;

      var additionalArgs = " " + options.additionalArgs;
      if (options.zooKeeper.length > 0) {
        additionalArgs += " -x " + options.zooKeeper;
      }


      var runCmd = ramcloudBinDir+"server"
        + " -C " +options.transport+":host="+coordinator+",port="+options.coordinatorPort
        + " -L " +options.transport+":host="+server+",port=" + options.clientPort
        + " --clusterName " + options.clusterName + " "
        + services
        + replicas
        + additionalArgs;

      runAndEmit(runCmd, server, username, password, "serverLogData", socket);
    }
  });

  socket.on('killServers', function(options) {
     for (i=options.serverStart; i<=options.serverEnd; i++) {
       var server = "00"+i;
        server = "192.168.0.1"+server.substr(server.length-2);

        var runCmd = "killall -9 server";
        runAndEmit(runCmd, server, username, password, "serverLogData", socket);
     }

     console.log(clientDir);
  })

  socket.on('clientCall', function(options) {
    var options = JSON.stringify(options, null, 2);
    fs.writeFile(clientDir+"options.json", options, function(err) {
      if (err) {
        console.log(err);
      } else {
        var runCmd = "cd " + clientDir + " && ./Client "+ coordinatorLocator +" options.json"
        console.log(runCmd);
        child.exec(runCmd);
      }
    });
  });

  socket.on('checkServerStatus', function(data) {
    if (data === undefined || data.beg === undefined || data.end === undefined)
      return;

    var emitAs = (data.emitAs === undefined) ? "checkServerStatusReturn" : data.emitAs;

    checkServerStatus(data.beg, data.end, function(list) {
      socket.emit(emitAs, list);
    });
  });
});

  var checkServerStatus = function(beg, end, callback) {
    if (beg > end || beg <= 0 || end > 80) return [];

    var fence = function (totalNum) {
      var completed = 0;
      var statusList = [];

      return function(serverNum) {
        return {
          onComplete:function(error, stdout, stderr) {
            var hostname = "rc" + ("00" + serverNum).slice(-2);
            var output = {hostname:hostname, num:serverNum, status:"Free", services:"", user:"", rcresInfo:""}
            if (error !== null) {
              output.status = "Error " + stderr;
            }

            var hasMaster = false;
            var hasCoordinator = false;
            var lines = stdout.split(/[\r\n]+/g);

            for (var i in lines) {
              var matches = lines[i].match(/([^ ]+).*coordinator/);
              if (matches != null) {
                hasCoordinator = true;
                output.user = matches[1]
                output.status = "Occupied"
              }

              matches = lines[i].match(/([^ ]+).*server/);
              if (matches != null) {
                hasMaster = true;
                output.user = matches[1];
                output.status = "Occupied"
              }
            }

            if (hasCoordinator && hasMaster) {
              output.services = "Coordinator, Master";
            } else if (hasCoordinator) {
              output.services = "Coordinator";
            } else if (hasMaster) {
              output.services = "Master";
            }

            statusList.push(output);

            completed++;
            if (completed == totalNum) {
              statusList.sort(function(a, b) {
                if (a.num > b.num) return 1;
                if (a.num < b.num) return -1;
                return 0;
              })

              // now add in rcres info too
              var cmd = "rcres ls -l";
              child.exec(cmd, function(error, stdout, stderr) {
                var lines = stdout.split(/[\r\n]+/g);
                for (var i in lines) {
                  var matches = lines[i].match(/([^ ])+  (rc[\d ]+)  \[(.*)\]/);
                  if (matches !== null) {
                    for (j in statusList) {
                      if (statusList[j].hostname == matches[2]) {
                        statusList[j].status += " LOCKED";
                        statusList[j].rcresInfo = matches[0];
                      }
                    }
                  }
                }

                callback(statusList)
              })
            }
          }
        }
      }
    }(end-beg+1);

    serverNum = beg;
    while (serverNum <= end) {
      var hostname = ("00" + serverNum).slice(-2);
      var remoteCmd = "/sbin/pidof coordinator > /dev/null  && /bin/ps u -p  $(/sbin/pidof coordinator) | tail -n 1; /sbin/pidof server >> /dev/null && /bin/ps u -p  $(/sbin/pidof server) | tail -n 1; ls > /dev/null"; // last ls is to prevent an error from being returned if the previous ps calls failed.
      var cmd = "ssh -o ConnectTimeout=1 rc"+hostname+" '"+remoteCmd+"'"
      child.exec(cmd, fence(serverNum).onComplete);
      serverNum++;
    }
  }

var runAndCallBack = function(command, server, username, password, callback) {
  console.log(server+": "+command);

  var log = "";
  var conn = new Connection();
  conn.on('ready', function() {
    var cmd =
      conn.exec(command, function(err, stream) {
        if (err) {
          log += "Error: " + err;
          callback(log);
        }
        stream.on('exit', function(code, signal) {
          log += "exited, code:"+code;
          callback(log);
        }).on('close', function() {
          conn.end();
        }).on('data', function(data) {
          log += data;
        }).stderr.on('data', function(data) {
          log+= data;
        });
      });
  }).on('error', function(err) {
    callback(err);
  }).connect({
    host: server,
    port: 22,
    username: username,
    password: password
  });
}

var runAndCallBackWhenDone = function(command, server, username, password, callback) {
  console.log(server+": "+command);
  var stderr = "";
  var stdout = "";

  var conn = new Connection();
  conn.on('ready', function() {
    var cmd = conn.exec(command, function(err, stream) {
      if (err) {
        var error = {code:code, signal:signal};
        callback(error, stdout, stderr);
      }
      stream.on('exit', function(code, signal) {
        var error = (code === 0) ? null : {code:code, signal:signal};
        callback(error, stdout, stderr);
      });

      stream.on('close', function() {
        conn.end();
      });

      stream.on('data', function(data) {
        stdout += data;
      });

      stream.stderr.on('data', function(data) {
        stderr += data;
      });
    });
  }).connect({
    host: server,
    port: 22,
    username: username,
    password: password
  });
}

var runAndEmit = function(command, server, username, password, reportAs, socket) {
  console.log(server+": "+command);
  try {
    var conn = new Connection();
    conn.on('ready', function() {
      conn.exec(command, function(err, stream) {
        if (err) {}
        stream.on('exit', function(code, signal) {
          if (reportAs !== undefined) {
            socket.emit(reportAs, {origin:server, text:"exited, code:"+code});
          }
        }).on('close', function() {
          conn.end();
        }).on('data', function(data) {
          if (reportAs !== undefined) {
            socket.emit(reportAs, {origin:server, text:""+data});
          }
        }).stderr.on('data', function(data) {
          if (reportAs !== undefined) {
            socket.emit(reportAs, {origin:server, text:""+data});
          }
        });
      });
    }).connect({
      host: server,
      port: 22,
      username: username,
      password: password
    })
  } catch(e) {
    console.log("host unreachable");
    socket.emit(reportAs, {origin:server, text:"Host unreachable: "+e});
  }
}

var coordinatorLogParserAndReporter = function(command, server, username, password, reportAs, socket) {
  console.log(server +": " + command);
  var reportAs = "logData";
  var clusterInformation = [];

  var conn = new Connection();
  conn.on('ready', function() {
    var cmd =
      conn.exec(command, function(err, stream) {
        stream.on('exit', function(code, signal) {
          if (reportAs !== undefined) {
            socket.emit(reportAs, {text:"exited, code:"+code});
          }
        }).on('close', function() {
          conn.end();
        }).on('data', function(data) {
          var string = ""+data;
          if (reportAs !== undefined) {
            socket.emit(reportAs, {text:string});
          }
        }).stderr.on('data', function(data) {
          var string = ""+data;
          socket.emit(reportAs, {text:string});
          if (reportAs !== undefined)
            var splitted = string.split(/[\r\n]+\d/g);
            for(i in splitted) {
              LogParser.interpretCoordinatorLogAsEventsAndEmit(splitted[i], "eventsLog", socket);
            }
        });
      });

  }).connect({
    host: server,
    port: 22,
    username: username,
    password: password
  });
}